variable "domain_name" {
  description = "certificate domain name"
}

variable "subdomain_name" {
  description = "certificate sub domain name"
}


variable "zone_id" {
  description = "zone id"
}

variable "route53_role" {
  default = "arn:aws:iam::559953719255:role/Route53-Legacy-DEV-Bambora-Hosted-Zones"
}

variable "region" {
  description = "The region that lambda will be created in"
  default = "ap-southeast-2"
}

variable "billing_tag" {
  description = "This could be use for billing purpose"
}

variable "created_by_tag" {
  description = "To identify the creator"
  default = "Terraform"
}

variable "certificate_count" {
  default = "1"
}