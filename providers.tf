provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

provider "aws" {
  alias  = "route53"
  region = "${var.region}"

  assume_role {
    role_arn = "${var.route53_role}"
  }
}
