
resource "aws_acm_certificate" "certificate" {
  provider          = "aws.us-east-1"
  domain_name       = "${var.subdomain_name}.${var.domain_name}"
  validation_method = "DNS"
  
  tags {
    Name        = "${var.subdomain_name}.${var.domain_name}"
    Region      = "${var.region}"
    Environment = "${terraform.workspace}"
    Project     = "${var.billing_tag}"
    CreatedBy   = "${var.created_by_tag}"
  }
  count = "${var.certificate_count}"
}



resource "aws_route53_record" "au_ssl" {
  zone_id = "${var.zone_id}"
  name    = "${aws_acm_certificate.certificate.0.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.certificate.0.domain_validation_options.0.resource_record_type}"
  ttl     = "3600"
  records = ["${aws_acm_certificate.certificate.0.domain_validation_options.0.resource_record_value}"]
  provider= "aws.route53"
  allow_overwrite = true
  count = "${var.certificate_count}"
}
